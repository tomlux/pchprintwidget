package lu.etat.pch.gis.widgets.pchPrintWidget.utils
{
	public class PrintUrl
	{
		private var _printUrl:String;
		private var _refererWebsite:String;
		private var _gpTask:Boolean=false;
		private var _label:String;
		private var _mxdFile:String;
		private var _physicalOutput:String;
		private var _virtualOutput:String;
		
		public function PrintUrl(value:Object)		{
			if((value.url+"").length>0) printUrl = value.url;
			else if((value.@url+"").length>0) printUrl = value.@url;
			if((value.@refererWebsite+"").length>0) refererWebsite = value.@refererWebsite;
			if((value.@gpTask+"").length>0) gpTask = value.@gpTask && value.@gpTask=='true';
			if((value.Label+"").length>0) label = value.Label;
			else if((value.@label+"").length>0) label = value.@label;
			if((value.mxdFile+"").length>0) mxdFile = value.mxdFile;
			if((value.physicalOutput+"").length>0) physicalOutput = value.physicalOutput;
			if((value.virtualOutput+"").length>0) virtualOutput = value.virtualOutput;
			if(!gpTask && printUrl.charAt(printUrl.length-1)!='/') {
				printUrl = printUrl+"/";
			}
		}
		
		public function get printUrl():String
		{
			return _printUrl;
		}

		public function set printUrl(value:String):void
		{
			_printUrl = value;
		}

		public function get refererWebsite():String
		{
			return _refererWebsite;
		}

		public function set refererWebsite(value:String):void
		{
			_refererWebsite = value;
		}

		public function get gpTask():Boolean
		{
			return _gpTask;
		}

		public function set gpTask(value:Boolean):void
		{
			_gpTask = value;
		}

		public function get label():String
		{
			return _label;
		}

		public function set label(value:String):void
		{
			_label = value;
		}

		public function get mxdFile():String
		{
			return _mxdFile;
		}

		public function set mxdFile(value:String):void
		{
			_mxdFile = value;
		}

		public function get physicalOutput():String
		{
			return _physicalOutput;
		}

		public function set physicalOutput(value:String):void
		{
			_physicalOutput = value;
		}

		public function get virtualOutput():String
		{
			return _virtualOutput;
		}

		public function set virtualOutput(value:String):void
		{
			_virtualOutput = value;
		}


	}
}