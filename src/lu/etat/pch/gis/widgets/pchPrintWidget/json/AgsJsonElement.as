package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    import com.esri.ags.geometry.Geometry;
    public class AgsJsonElement {
        public function AgsJsonElement(geometry:Geometry=null, symbol:Object=null, anchor:String="bottomleft", xOffset:Number=0, yOffset:Number=0, name:String=null, visible:Boolean=true, width:String="", height:String="",order:Number=0) {
            this.geometry=geometry;
            this.symbol=symbol;
            this.anchor=anchor;
			this.xOffset=xOffset;
			this.yOffset=yOffset;
            this.name=name;
            this.visible=visible;
			this.width=width;
			this.height=height;
			this.order=order;
        }

        private var _anchor:String;
        private var _geometry:Geometry;
        private var _name:String;
        private var _symbol:Object; //AgsJsonSymbol+TextSymbol
		private var _xOffset:Number;
		private var _yOffset:Number;		
        private var _visible:Boolean;
		private var _width:String;
		private var _height:String;
		private var _order:Number;

		public function get yOffset():Number
		{
			return _yOffset;
		}

		public function set yOffset(value:Number):void
		{
			_yOffset = value;
		}

		public function get xOffset():Number
		{
			return _xOffset;
		}

		public function set xOffset(value:Number):void
		{
			_xOffset = value;
		}

        public function get anchor():String {
            return _anchor;
        }

        public function set anchor(value:String):void {
            _anchor=value;
        }

        public function get geometry():Geometry {
            return _geometry;
        }

        public function set geometry(value:Geometry):void {
            _geometry=value;
        }

        public function get name():String {
            return _name;
        }

        public function set name(value:String):void {
            _name=value;
        }

        public function get symbol():Object {
            return _symbol;
        }

        public function set symbol(value:Object):void {
            _symbol=value;
        }

        public function get visible():Boolean {
            return _visible;
        }

        public function set visible(value:Boolean):void {
            _visible=value;
        }
		
		public function get height():String {
			return _height;
		}
		
		public function set height(value:String):void {
			_height=value;
		}
		
		public function get width():String {
			return _width;
		}
		
		public function set width(value:String):void {
			_width=value;
		}

		public function get order():Number
		{
			return _order;
		}

		public function set order(value:Number):void
		{
			_order = value;
		}

    }
}