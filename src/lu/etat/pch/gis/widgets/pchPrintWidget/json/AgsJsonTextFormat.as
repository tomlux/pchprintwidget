package lu.etat.pch.gis.widgets.pchPrintWidget.json {
	import flash.text.TextFormat;

    public class AgsJsonTextFormat {
        public function AgsJsonTextFormat(family:String="Arial", size:Number=12, style:String="normal", color:AgsJsonRGBColor=null,weight:String="normal", decoration:String="none") {
            this._family=family;
            this._size=size;
            this._style=style;
			this._color=color;
            this._weight=weight;
            this._decoration=decoration;
        }
		/*
		"font" : {
		"family" : "<fontFamily>",
		"size" : <fontSize>,
		"style" : "<italic | normal | oblique>",
		"weight" : "<bold | bolder | lighter | normal>",
		"decoration" : "<line-through | underline | none>"
		}
		*/

		public static function getObjectFromTextFormat(tf:TextFormat):AgsJsonTextFormat {
			if(tf==null)
				return null;
			var font:String = tf.font;
			var color:AgsJsonRGBColor = AgsJsonRGBColor.getObjectFromRGBhex(uint(tf.color));
			var size:Number = tf.size as Number;
			var style:String ="normal";
			var weight:String ="normal";
			var decoration:String ="none";
			if(tf.italic) style="italic";
			if(tf.bold) weight="bold";
			if(tf.underline) decoration="underline";
			/*todo: complete me
			"style" : "< oblique>",
			"weight" : "< bolder | lighter >",
			"decoration" : "< line-through >"
			*/
			var retSym:AgsJsonTextFormat=new AgsJsonTextFormat(font,size,style,color,weight,decoration);
			return retSym;
		}

        private var _decoration:String;
        private var _family:String;
		private var _size:Number;
		private var _color:AgsJsonRGBColor;
        private var _style:String;
        private var _weight:String;

        public function get decoration():String {
            return _decoration;
        }

        public function set decoration(value:String):void {
            _decoration=value;
        }

        public function get family():String {
            return _family;
        }

        public function set family(value:String):void {
            _family=value;
        }

        public function get size():Number {
            return _size;
        }

        public function set size(value:Number):void {
            _size=value;
        }

        public function get style():String {
            return _style;
        }

        public function set style(value:String):void {
            _style=value;
        }

        public function get weight():String {
            return _weight;
        }

        public function set weight(value:String):void {
            _weight=value;
        }

		public function get color():AgsJsonRGBColor
		{
			return _color;
		}

		public function set color(value:AgsJsonRGBColor):void
		{
			_color = value;
		}

		
    }
}