package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    /**
     * 
     * @author schullto
     */
    public class AgsJsonNumericFormat {
		
        /**
         * 
         * @param roundingOption
         * @param roundingValue
         */
        public function AgsJsonNumericFormat(roundingOption:Number=0, roundingValue:Number=3) {
            this.roundingOption=roundingOption;
            this.roundingValue=roundingValue;
        }

        private var _roundingOption:Number;
        private var _roundingValue:Number;
		private var _alignmentOption:Number;
		private var _alignmentWidth:Number;
		private var _showPlusSign:Boolean;
		private var _useSeparator:Boolean;
		private var _zeroPad:Boolean;

        /**
		 * esriRoundingOptionEnum:
         *   int esriRoundNumberOfDecimals = 0;
         *   int esriRoundNumberOfSignificantDigits = 1;
         * @return 
         */
        public function get roundingOption():Number {
            return _roundingOption;
        }

        /**
		 * esriRoundingOptionEnum:
         *   int esriRoundNumberOfDecimals = 0;
         *   int esriRoundNumberOfSignificantDigits = 1;
         * @param value
         */
        public function set roundingOption(value:Number):void {
            _roundingOption=value;
        }

        /**
         * 
         * @return 
         */
        public function get roundingValue():Number {
            return _roundingValue;
        }

        /**
         * 
         * @param value
         */
        public function set roundingValue(value:Number):void {
            _roundingValue=value;
        }

		public function get alignmentOption():Number
		{
			return _alignmentOption;
		}

		public function set alignmentOption(value:Number):void
		{
			_alignmentOption = value;
		}

		public function get alignmentWidth():Number
		{
			return _alignmentWidth;
		}

		public function set alignmentWidth(value:Number):void
		{
			_alignmentWidth = value;
		}

		public function get showPlusSign():Boolean
		{
			return _showPlusSign;
		}

		public function set showPlusSign(value:Boolean):void
		{
			_showPlusSign = value;
		}

		public function get useSeparator():Boolean
		{
			return _useSeparator;
		}

		public function set useSeparator(value:Boolean):void
		{
			_useSeparator = value;
		}

		public function get zeroPad():Boolean
		{
			return _zeroPad;
		}

		public function set zeroPad(value:Boolean):void
		{
			_zeroPad = value;
		}


    }
}